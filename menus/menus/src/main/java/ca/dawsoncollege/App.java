package ca.dawsoncollege;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        boolean running = true;
        while(running){
            DisplayMenu();
            var input = System.console().readLine("Select an option: ");
            switch (input) {
                case "1":
                    Connect();
                    Display();
                    break;
                case "2":
                System.out.println("Exiting");
                    running = false;
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
            }
        }
    }

    private static void Display() {
        boolean running = true;
        while(running){
            System.out.println("Display Menu");
            System.out.println("1. Display Courses");
            System.out.println("2. Display Competencies");
            System.out.println("3. Back");
            var input = System.console().readLine("Select an option: ");
            switch (input) {
                case "1":
                try {
                    DisplayCourses();
                } catch (Exception e) {
                    System.out.println("Something went wrong display courses, try again");
                    running = false;
                }
                    break;
                case "2":
                    break;
                case "3":
                    running = false;
                    break;
                default:
                    System.out.println("Invalid option");
                    break;
            }
        }
    }

    private static void DisplayCourses() throws Exception {
        throw new Exception("This didn't work!");
    }

    private static void Connect() {

    }

    private static void DisplayMenu() {
        System.out.println("Welcome to our Project");
        System.out.println("1. Connect");
        System.out.println("2. Exit");
    }
}
