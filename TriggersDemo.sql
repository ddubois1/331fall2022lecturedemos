create table BookLogs (
    log_user varchar2(100),
    change_time timestamp,
    msg varchar2(100)
);

create or replace trigger after_books_updated After update on Books for each row
begin
    insert into BookLogs values (USER, current_timestamp, 'updated: ' || :NEW.isbn);
end;
/
create trigger after_books_inserted After insert on Books for each row
begin
    insert into BookLogs values (USER, current_timestamp, 'inserted');
end;
/
create trigger after_books_deleted After delete on Books for each row
begin
    insert into BookLogs values (USER, current_timestamp, 'deleted');
end;
/
select * from books;
select * from BookLogs;
update books set cost=10 where category='COOKING';