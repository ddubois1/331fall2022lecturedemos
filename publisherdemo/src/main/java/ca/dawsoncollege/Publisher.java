package ca.dawsoncollege;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Publisher implements SQLData {
    public static final String TYPENAME = "PUB_TYP";

    private int pubid;
    private String name;
    private String contact;
    private String phone;

    public Publisher(){
        
    }

    public Publisher(int pubid, String name, 
    String contact, String phone){
        this.pubid = pubid;
        this.name = name;
        this.contact = contact;
        this.phone = phone;
    }

    public int getPubid() {
        return pubid;
    }

    public void setPubid(int pubid) {
        this.pubid = pubid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        
        return Publisher.TYPENAME;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setPubid(stream.readInt());
        setName(stream.readString());
        setContact(stream.readString());
        setPhone(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeInt(getPubid());
        stream.writeString(getName());
        stream.writeString(getContact());
        stream.writeString(getPhone());
    }

    @Override
    public String toString() {
        return "Publisher [pubid=" + pubid + ", name=" + name + ", contact=" + contact + ", phone=" + phone + "]";
    }
}
