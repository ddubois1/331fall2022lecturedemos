package ca.dawsoncollege;

import java.sql.Date;
import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Book implements SQLData {
    public static final String TYPENAME = "BOOK_TYP";

    private String isbn;
    private String title;
    private Date date;
    private Publisher publisher;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    public Book() {
        
    }
    public Book(String isbn, String title, Date date, 
    Publisher publisher, double cost, double retail, 
    double discount, String category) {
        this.isbn = isbn;
        this.title = title;
        this.date = date;
        this.publisher = publisher;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
    }
    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public Publisher getPublisher() {
        return publisher;
    }
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    public double getRetail() {
        return retail;
    }
    public void setRetail(double retail) {
        this.retail = retail;
    }
    public double getDiscount() {
        return discount;
    }
    public void setDiscount(double discount) {
        this.discount = discount;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    @Override
    public String toString() {
        return "Book [isbn=" + isbn + ", title=" + title + ", date=" + date + ", publisher=" + publisher + ", cost="
                + cost + ", retail=" + retail + ", discount=" + discount + ", category=" + category + "]";
    }
    @Override
    public String getSQLTypeName() throws SQLException {
        return Book.TYPENAME;
    }
    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        setIsbn(stream.readString());
        setTitle(stream.readString());
        setDate(stream.readDate());
        setPublisher((Publisher)stream.readObject());
        setCost(stream.readDouble());
        setRetail(stream.readDouble());
        setDiscount(stream.readDouble());
        setCategory(stream.readString());
    }
    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        stream.writeString(getIsbn());
        stream.writeString(getTitle());
        stream.writeDate(getDate());
        stream.writeObject(getPublisher());
        stream.writeDouble(getCost());
        stream.writeDouble(getRetail());
        stream.writeDouble(getDiscount());
        stream.writeString(getCategory());
    }
}
