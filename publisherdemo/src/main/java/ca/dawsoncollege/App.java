package ca.dawsoncollege;
import java.sql.*;
import java.util.Map;
/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        String user = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try(Connection conn = DriverManager.getConnection(url, 
        user, password)){
            Map map = conn.getTypeMap();
            map.put(Publisher.TYPENAME, 
            Class.forName("ca.dawsoncollege.Publisher"));
            map.put(Book.TYPENAME, 
            Class.forName("ca.dawsoncollege.Book"));
            conn.setTypeMap(map);

            String addProcBook = "{ call add_book(?)}";
            try(CallableStatement stmt = conn.prepareCall(addProcBook)){
                Publisher publisher = new Publisher(7, "John", "Smith", "555555");
                Book book = new Book("bbb", "Another book", 
                Date.valueOf("2022-12-14"), 
                publisher, 0, 0, 0, "scifi");
                stmt.setObject(1, book);
                stmt.execute();
            }

            String addProc = "{ call add_publisher(?)}";
            try(CallableStatement stmt = conn.prepareCall(addProc)){
                Publisher publisher = new Publisher(7, "John", "Smith", "555555");
                stmt.setObject(1, publisher);
                stmt.execute();
            }

            String getProc = "{? = call get_publisher(?)}";
            try(CallableStatement stmt = conn.prepareCall(getProc)){
                stmt.registerOutParameter(1, Types.STRUCT, 
                Publisher.TYPENAME);
                stmt.setInt(2, 1);
                stmt.execute();
                Publisher pub = (Publisher)stmt.getObject(1);
                System.out.println(pub.toString());
            }
        }
    }
}
