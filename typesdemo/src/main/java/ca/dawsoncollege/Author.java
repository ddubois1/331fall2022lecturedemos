package ca.dawsoncollege;

import java.sql.SQLData;
import java.sql.SQLException;
import java.sql.SQLInput;
import java.sql.SQLOutput;

public class Author implements SQLData {
    private String authorId;
    private String lname;
    private String fname;
    private String typeName;

    public Author(){
        
    }

    public Author(String authorId, String lname, String fname) {
        this.authorId = authorId;
        this.lname = lname;
        this.fname = fname;
        typeName = "AUTHOR_TYP";
    }

    public String getAuthorId() {
        return authorId;
    }

    public void setAuthorId(String authorId) {
        this.authorId = authorId;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    @Override
    public String getSQLTypeName() throws SQLException {
        return typeName;
    }

    @Override
    public void readSQL(SQLInput stream, String typeName) throws SQLException {
        this.typeName = typeName;
        setAuthorId(stream.readString());
        setLname(stream.readString());
        setFname(stream.readString());
    }

    @Override
    public void writeSQL(SQLOutput stream) throws SQLException {
        //Note order here matters, it must match the type
        stream.writeString(getAuthorId());
        stream.writeString(getLname());
        stream.writeString(getFname());
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return getLname();
    }
}
