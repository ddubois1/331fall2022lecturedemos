package ca.dawsoncollege;

import java.util.Map;

import oracle.jdbc.OracleTypes;

import java.sql.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws SQLException, ClassNotFoundException
    {
        String user = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try(Connection conn = DriverManager.getConnection(url, 
        user, password)){
            Map map = conn.getTypeMap();
            map.put("AUTHOR_TYP", Class.forName("ca.dawsoncollege.Author"));
            conn.setTypeMap(map);
            
            Author myAuthor = new Author("D100", "Dubois", "Dirk");
            String sql = "{call add_author(?)}";
            try(CallableStatement stmt = conn.prepareCall(sql)){
                stmt.setObject(1, myAuthor);
                stmt.execute();
            }
            
            sql = "{? = call get_author(?)}";
            try(CallableStatement stmt = conn.prepareCall(sql)){
                stmt.registerOutParameter(1, Types.STRUCT, "AUTHOR_TYP");
                stmt.setString(2, "S100");
                stmt.execute();
                Author newAuthor = (Author)stmt.getObject(1);
                System.out.println(newAuthor.toString());
            }
        }
    }
}
