DROP PROCEDURE add_publisher;

DROP TYPE pub_typ;
/

CREATE OR REPLACE TYPE pub_typ AS OBJECT (
    pubid    NUMBER(2),
    name     VARCHAR2(23),
    contact  VARCHAR2(15),
    phone    VARCHAR2(12)
);
/

CREATE OR REPLACE TYPE book_typ AS OBJECT (
    isbn      VARCHAR2(10),
    title     VARCHAR2(30),
    pubdate   DATE,
    pub       pub_typ,
    cost      NUMBER(5, 2),
    retail    NUMBER(5, 2),
    discount  NUMBER(4, 2),
    category  VARCHAR2(12)
);
/

CREATE OR REPLACE PROCEDURE add_book (
    vbook IN book_typ
) AS
BEGIN
    INSERT INTO books VALUES (
        vbook.isbn,
        vbook.title,
        vbook.pubdate,
        vbook.pub.pubid,
        vbook.cost,
        vbook.retail,
        vbook.discount,
        vbook.category
    );

END;
/

CREATE OR REPLACE PROCEDURE add_publisher (
    vpub IN pub_typ
) AS
BEGIN
    INSERT INTO publisher VALUES (
        vpub.pubid,
        vpub.name,
        vpub.contact,
        vpub.phone
    );

EXCEPTION
    WHEN dup_val_on_index THEN
        UPDATE publisher
        SET
            name = vpub.name,
            contact = vpub.contact,
            phone = vpub.phone
        WHERE
            pubid = vpub.pubid;

END;
/

CREATE OR REPLACE FUNCTION get_publisher (
    vpubid NUMBER
) RETURN pub_typ AS

    vname     publisher.name%TYPE;
    vcontact  publisher.contact%TYPE;
    vphone    publisher.phone%TYPE;
    vpub      pub_typ;
BEGIN
    SELECT
        name,
        contact,
        phone
    INTO
        vname,
        vcontact,
        vphone
    FROM
        publisher
    WHERE
        pubid = vpubid;

    vpub := pub_typ(vpubid, vname, vcontact, vphone);
    RETURN vpub;
END;
/

DECLARE
    vbook  book_typ;
    vpub   pub_typ;
BEGIN
    vpub := pub_typ(7, 'Jane', 'Smith', '111');
    vbook := book_typ('aaa', 'My title', sysdate, vpub, 10,
                     5, 1, 'scifi');
    add_book(vbook);
END;